<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>strong_The whatsapp must be between 10 and 12 digits</name>
   <tag></tag>
   <elementGuidId>3300e60c-edb6-4802-8d5c-12581b1f5761</elementGuidId>
   <selectorCollection>
      <entry>
         <key>BASIC</key>
         <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;py-4&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;invalid-feedback&quot;]/strong[1][count(. | //strong[(text() = 'The whatsapp must be between 10 and 12 digits.' or . = 'The whatsapp must be between 10 and 12 digits.')]) = count(//strong[(text() = 'The whatsapp must be between 10 and 12 digits.' or . = 'The whatsapp must be between 10 and 12 digits.')])]</value>
      </entry>
      <entry>
         <key>CSS</key>
         <value>div.invalid-feedback > strong</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div[2]/div[3]/div/strong</value>
      </entry>
   </selectorCollection>
   <selectorMethod>BASIC</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>strong</value>
      <webElementGuid>d0116f34-73b1-4986-9801-227a885081e7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>The whatsapp must be between 10 and 12 digits.</value>
      <webElementGuid>36530d9e-5b5d-4a30-8f36-c9d94f33e7c3</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;app&quot;)/div[@class=&quot;main-wrapper main-wrapper-1&quot;]/div[@class=&quot;main-content&quot;]/section[@class=&quot;section&quot;]/div[@class=&quot;section-body&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-12&quot;]/div[@class=&quot;card author-box&quot;]/div[@class=&quot;row justify-content-center&quot;]/div[@class=&quot;col-12 col-md-12 col-lg-6&quot;]/div[@class=&quot;card-body&quot;]/form[1]/div[@class=&quot;py-4&quot;]/div[@class=&quot;form-group&quot;]/div[@class=&quot;invalid-feedback&quot;]/strong[1]</value>
      <webElementGuid>c9da62cf-76d7-4428-a067-7abc32783010</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='app']/div/div[3]/section/div/div/div/div/div/div/div/form/div[2]/div[3]/div/strong</value>
      <webElementGuid>fc27ea72-13fb-4bf1-bb1b-fda796309373</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Phone'])[1]/following::strong[1]</value>
      <webElementGuid>a642800b-b1fe-4346-b0a1-42c796a26e84</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Email'])[1]/following::strong[1]</value>
      <webElementGuid>b40fed32-7716-47e3-979e-1092c14b8e2e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='BirthDay'])[1]/preceding::strong[1]</value>
      <webElementGuid>5c672926-5d77-4160-8fc9-908d0daa0f82</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Cancel'])[1]/preceding::strong[1]</value>
      <webElementGuid>38848cf6-bcc0-4847-9068-4069e6a2f78e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='The whatsapp must be between 10 and 12 digits.']/parent::*</value>
      <webElementGuid>16bdb521-194e-4ba7-a50d-d7c3048b326d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/strong</value>
      <webElementGuid>1e68c05a-3a2d-43a6-a851-ed8a6cf66d61</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//strong[(text() = 'The whatsapp must be between 10 and 12 digits.' or . = 'The whatsapp must be between 10 and 12 digits.')]</value>
      <webElementGuid>3c14d8a0-cf79-4c0f-b8bc-d7994d6d2f56</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
