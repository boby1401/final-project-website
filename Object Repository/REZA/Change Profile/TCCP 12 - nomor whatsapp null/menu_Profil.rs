<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>menu_Profil</name>
   <tag></tag>
   <elementGuidId>91afb0ae-12d7-4d85-aee6-71ee11a4f8ab</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>a.nav-link.toggled</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//aside[@id='sidebar-wrapper']/ul/li[4]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>8b0a04a9-cebc-4636-a2aa-fe7dc2c24dc7</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/dashboard/profile</value>
      <webElementGuid>5b6d49ab-6790-44ae-a5b9-00500a0e6ed4</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>nav-link toggled</value>
      <webElementGuid>f7a3d164-3a6c-46da-bd55-0b10852e9d46</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Profil</value>
      <webElementGuid>ef05e302-eeba-404c-bb3f-7cde788447db</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;sidebar-wrapper&quot;)/ul[@class=&quot;sidebar-menu&quot;]/li[@class=&quot;dropdown     active&quot;]/a[@class=&quot;nav-link toggled&quot;]</value>
      <webElementGuid>36c79a7d-3904-4439-94bc-83f066a7e149</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//aside[@id='sidebar-wrapper']/ul/li[4]/a</value>
      <webElementGuid>bdb62154-c5c8-408f-8d73-56f6750413f4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Basic Info'])[1]/following::a[1]</value>
      <webElementGuid>f8287aa0-46f5-430e-8af6-ab23be8e1499</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Dashboard'])[1]/following::a[1]</value>
      <webElementGuid>0d64730f-908e-44ef-9a72-41482095de13</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Billing Account'])[1]/preceding::a[1]</value>
      <webElementGuid>208c7ebc-7313-440c-8941-df6375079156</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/dashboard/profile')]</value>
      <webElementGuid>3042287d-e3ef-424a-a346-9408e70bc0b9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[4]/a</value>
      <webElementGuid>43f3b551-5281-4ae4-90c2-806b7f7f1874</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/dashboard/profile' and (text() = 'Profil' or . = 'Profil')]</value>
      <webElementGuid>ce3fb348-3bb4-4503-99ec-23d1c527547e</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
