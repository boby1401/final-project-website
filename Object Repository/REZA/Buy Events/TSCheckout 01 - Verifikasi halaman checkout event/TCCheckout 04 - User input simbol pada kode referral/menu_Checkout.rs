<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>menu_Checkout</name>
   <tag></tag>
   <elementGuidId>077ef1cf-a1fa-4a55-8f57-3cf553068f89</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//div[@id='navbar-collapse-1']/ul/li[7]/ul/li[2]/a</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>6eff86ce-5820-454b-acc9-0e593a6f848d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/view_cart</value>
      <webElementGuid>c1da7912-da3c-4713-a318-9dc88bf09716</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Checkout</value>
      <webElementGuid>8e0361d4-93ca-47fc-b038-bb24db4f3d3f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;navbar-collapse-1&quot;)/ul[@class=&quot;nav navbar-nav&quot;]/li[@class=&quot;___class_+?26___&quot;]/ul[@class=&quot;wm-dropdown-menu&quot;]/li[2]/a[1]</value>
      <webElementGuid>0f696bfb-b9d8-464b-96b1-a58ed820cf45</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='navbar-collapse-1']/ul/li[7]/ul/li[2]/a</value>
      <webElementGuid>da83c897-c7d5-47a3-af15-ed107bf4effd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Checkout')]</value>
      <webElementGuid>9a41ffa1-13ec-40cc-b6b3-85f947ab75f8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Kontak'])[1]/following::a[3]</value>
      <webElementGuid>caf0010b-2422-4eaa-8b70-139cd4bca10c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='My Account'])[1]/preceding::a[1]</value>
      <webElementGuid>515a9c80-bc19-4ddf-867f-7afe7c2b7e47</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Logout'])[1]/preceding::a[2]</value>
      <webElementGuid>2dfcc2a0-d25e-426c-bed5-e5b3359a37c9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Checkout']/parent::*</value>
      <webElementGuid>a2f6171e-c265-4f03-a9ec-1acbb3a3e7a1</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/view_cart')]</value>
      <webElementGuid>9a97e255-de46-4831-9d59-50a1aa3109dc</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//li[7]/ul/li[2]/a</value>
      <webElementGuid>ef98845a-aac5-431c-8a98-bc999d74b174</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/view_cart' and (text() = 'Checkout' or . = 'Checkout')]</value>
      <webElementGuid>967b9954-f4fd-4652-b32c-ec8d9d7ad649</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
