<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>input_cekbox</name>
   <tag></tag>
   <elementGuidId>7ac3efb6-f0c9-4fe1-9911-65868e3f51aa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#check</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//input[@id='check']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>input</value>
      <webElementGuid>06768910-7ab0-4d95-9ed8-414527052d0e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>9baef04e-ff3d-48f1-9d81-737941c8032e</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>value</name>
      <type>Main</type>
      <value>85000|129|1</value>
      <webElementGuid>79c13097-ba13-4903-a0b6-450e67f0e958</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>checkbox</value>
      <webElementGuid>ac50f6ea-3658-4e40-81ea-a3ae5f11b641</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>name</name>
      <type>Main</type>
      <value>Day 3: Predict using Machine Learning</value>
      <webElementGuid>d329f51c-a4f4-45f5-89b2-03870e6bdc41</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>data</name>
      <type>Main</type>
      <value>500000</value>
      <webElementGuid>4c2b2aca-f300-4a2b-b391-c458c7f6de21</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>check</value>
      <webElementGuid>abf257f8-05ca-45a6-99ec-2b479b53b62d</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;check&quot;)</value>
      <webElementGuid>a04650d5-6510-4014-a7ff-bda14b640981</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//input[@id='check']</value>
      <webElementGuid>fcc62c74-4e81-4901-948c-d68717721a83</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div/div/div/div/input</value>
      <webElementGuid>b8b41872-e1ab-4765-9646-03ccf17d9edf</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div/div/input</value>
      <webElementGuid>1ccb3fe2-63cc-42ce-bc8c-93e2cf437244</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//input[@type = 'checkbox' and @name = 'Day 3: Predict using Machine Learning' and @id = 'check']</value>
      <webElementGuid>287fae40-1d40-420b-bc03-bf0d3f91cb84</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
