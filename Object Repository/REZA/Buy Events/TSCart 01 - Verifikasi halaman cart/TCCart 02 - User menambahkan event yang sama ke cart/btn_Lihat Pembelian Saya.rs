<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>btn_Lihat Pembelian Saya</name>
   <tag></tag>
   <elementGuidId>a52a06e2-4bc4-4b8b-8956-2c2bb48f6cf2</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#checkoutButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//a[@id='checkoutButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>a</value>
      <webElementGuid>ac9f0c73-b4fd-4be3-b07a-8acd0205d249</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>href</name>
      <type>Main</type>
      <value>/view_cart/129</value>
      <webElementGuid>1f883604-a24a-41bf-9c51-0ddc894c661f</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkoutButton</value>
      <webElementGuid>75ed3e58-f313-4e11-8826-64c10fd7fef1</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Lihat
                Pembelian Saya</value>
      <webElementGuid>7877092b-ab34-4ddc-b4ca-e846355d065a</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkoutButton&quot;)</value>
      <webElementGuid>2d340f2d-7585-4ec8-a438-7d474db9cb4a</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//a[@id='checkoutButton']</value>
      <webElementGuid>078c6e06-715e-4098-9318-fd875a69a417</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']/div/div[2]/a</value>
      <webElementGuid>58fc5da1-6e9b-4a3b-bbef-848492cf5b0d</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:link</name>
      <type>Main</type>
      <value>//a[contains(text(),'Lihat
                Pembelian Saya')]</value>
      <webElementGuid>57cb05bf-258c-4553-8670-ce6e56f46a77</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 3: Predict using Machine Learning'])[3]/following::a[1]</value>
      <webElementGuid>b06870b1-921d-4951-b637-96968741669e</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='add to cart success'])[1]/following::a[1]</value>
      <webElementGuid>bc41b74c-c4bf-4385-a9a8-c72dd8c4184c</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Lihat Event Lainnya'])[1]/preceding::a[1]</value>
      <webElementGuid>6f215bd4-8237-4051-ab49-28d564ffdca6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Copyright © 2010-2021 Coding.ID All rights reserved.'])[1]/preceding::a[2]</value>
      <webElementGuid>73fc1c88-871e-4429-861a-74eaede4a5e6</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:href</name>
      <type>Main</type>
      <value>//a[contains(@href, '/view_cart/129')]</value>
      <webElementGuid>9c6ffcde-38c0-40be-9ba8-b3842440d664</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[6]/div/div[2]/a</value>
      <webElementGuid>5fa60ddd-501d-4ead-8da0-5fa23528f6cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//a[@href = '/view_cart/129' and @id = 'checkoutButton' and (text() = 'Lihat
                Pembelian Saya' or . = 'Lihat
                Pembelian Saya')]</value>
      <webElementGuid>992875f3-9ae2-4941-bd05-04834cd3cc06</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
