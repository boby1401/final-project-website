<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>h3_add to cart success</name>
   <tag></tag>
   <elementGuidId>fbf7f919-e961-4076-adb1-bbcbc6d96daa</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#info</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//h3[@id='info']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>h3</value>
      <webElementGuid>cac37872-0c66-4f2a-831d-0d4b04312283</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>info</value>
      <webElementGuid>ae6c01ad-cfe5-41ec-8ae8-9bc4d2fb8e79</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>add to cart success</value>
      <webElementGuid>8aeeff78-fdbc-4c4c-a514-cc379453e9ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;info&quot;)</value>
      <webElementGuid>3e789a1b-e7b5-49bc-bfa5-cd23a78ee49b</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//h3[@id='info']</value>
      <webElementGuid>ac82f3e8-a992-404f-a675-199aad3aecce</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//div[@id='Modal_Success']/div/div[2]/h3</value>
      <webElementGuid>cfefe9f0-98ce-4315-ac82-6529a8fc5632</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Tutup'])[2]/following::h3[1]</value>
      <webElementGuid>e2242031-4b27-4e58-8474-9280f18e2cf8</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Day 3: Predict using Machine Learning'])[3]/preceding::h3[1]</value>
      <webElementGuid>97bd6109-3017-4264-a5db-0c3f762b45bd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='add to cart success']/parent::*</value>
      <webElementGuid>f917043a-2691-4e8c-abf3-7879ff36b5a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/h3</value>
      <webElementGuid>0f077f43-f784-41ad-8381-177915f2aeb5</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//h3[@id = 'info' and (text() = 'add to cart success' or . = 'add to cart success')]</value>
      <webElementGuid>404bf22d-67b0-4cfb-8341-65863702ab5d</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
