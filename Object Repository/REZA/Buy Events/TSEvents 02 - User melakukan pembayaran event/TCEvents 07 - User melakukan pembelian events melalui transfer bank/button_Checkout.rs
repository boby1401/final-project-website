<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>button_Checkout</name>
   <tag></tag>
   <elementGuidId>506f8e34-0535-468a-9cbf-571405a85318</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>#checkoutButton</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//button[@id='checkoutButton']</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>419e4da6-1f7b-41c3-aec2-a286514211fc</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>type</name>
      <type>Main</type>
      <value>button</value>
      <webElementGuid>4aa09f24-39b7-4f66-97fd-b4ceb743a5ec</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>id</name>
      <type>Main</type>
      <value>checkoutButton</value>
      <webElementGuid>0cb13d11-e4bc-49d9-a658-86eb46923bc2</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>Checkout</value>
      <webElementGuid>25d6c701-1683-4219-aeae-1c2461391196</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;checkoutButton&quot;)</value>
      <webElementGuid>d341daf8-d34b-43d5-9f72-22aba85ffe23</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:attributes</name>
      <type>Main</type>
      <value>//button[@id='checkoutButton']</value>
      <webElementGuid>eb4c347c-8604-4201-ae2c-19774b335d06</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='cartForm']/div[2]/div/button</value>
      <webElementGuid>579805aa-8cd0-417c-b295-f767387fcd9a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Rp 85.000'])[2]/following::button[1]</value>
      <webElementGuid>cc3b4558-1ef3-4602-aa1c-19dafb0c66cb</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total'])[1]/following::button[1]</value>
      <webElementGuid>36b68b5f-0ca9-4f85-a360-072086ffc19a</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Apply'])[1]/preceding::button[1]</value>
      <webElementGuid>75fd7b2b-5f66-44f4-830d-4df367f3f3f9</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Riwayat Pembelian'])[1]/preceding::button[2]</value>
      <webElementGuid>040226d6-55ea-4714-8ab9-fa15feb77147</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//div[2]/div/button</value>
      <webElementGuid>1791ec43-1b7e-4428-ac3c-bf6d4fee5116</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//button[@type = 'button' and @id = 'checkoutButton' and (text() = 'Checkout' or . = 'Checkout')]</value>
      <webElementGuid>916d320a-350e-4170-b9fd-2aec68dfffa8</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
