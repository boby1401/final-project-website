<?xml version="1.0" encoding="UTF-8"?>
<WebElementEntity>
   <description></description>
   <name>td_Transfer QR Code  Bank (cek otomatis)</name>
   <tag></tag>
   <elementGuidId>06871cde-9367-49de-8474-81dcf4aeeb07</elementGuidId>
   <selectorCollection>
      <entry>
         <key>CSS</key>
         <value>td.product-col</value>
      </entry>
      <entry>
         <key>XPATH</key>
         <value>//form[@id='modalform']/table[2]/tbody/tr/td[2]</value>
      </entry>
   </selectorCollection>
   <selectorMethod>XPATH</selectorMethod>
   <useRalativeImagePath>true</useRalativeImagePath>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>tag</name>
      <type>Main</type>
      <value>td</value>
      <webElementGuid>1a9f9c28-3056-48ef-ad21-f3e9cfab8794</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>class</name>
      <type>Main</type>
      <value>product-col</value>
      <webElementGuid>4443ef8e-5bb6-4dd9-aacc-8cc7b7481396</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>text</name>
      <type>Main</type>
      <value>
                                            Transfer QR Code / Bank (cek otomatis)
                                        </value>
      <webElementGuid>4291fd5e-0dbe-428f-8eb9-3484fce3bb55</webElementGuid>
   </webElementProperties>
   <webElementProperties>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath</name>
      <type>Main</type>
      <value>id(&quot;modalform&quot;)/table[2]/tbody[1]/tr[1]/td[@class=&quot;product-col&quot;]</value>
      <webElementGuid>c5b18c0a-d800-47ce-9ebf-250eb3446ac8</webElementGuid>
   </webElementProperties>
   <webElementXpaths>
      <isSelected>true</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:idRelative</name>
      <type>Main</type>
      <value>//form[@id='modalform']/table[2]/tbody/tr/td[2]</value>
      <webElementGuid>7d772b58-6c60-4693-b242-9a6265e341da</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Total Pembayaran'])[1]/following::td[3]</value>
      <webElementGuid>39bf2e2a-a990-4f5d-b06f-3a3107c4c497</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='-'])[2]/following::td[4]</value>
      <webElementGuid>3c510e26-d724-40bc-8a03-666de9bd84a4</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>(.//*[normalize-space(text()) and normalize-space(.)='Confirm'])[1]/preceding::td[5]</value>
      <webElementGuid>54924de4-ada4-437e-beae-5afa5ac170fd</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:neighbor</name>
      <type>Main</type>
      <value>//*/text()[normalize-space(.)='Transfer QR Code / Bank (cek otomatis)']/parent::*</value>
      <webElementGuid>42be29a5-7958-4276-83c1-83dad8230504</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:position</name>
      <type>Main</type>
      <value>//table[2]/tbody/tr/td[2]</value>
      <webElementGuid>6aaa86e8-518f-4d23-b9ac-e40acb1cc66b</webElementGuid>
   </webElementXpaths>
   <webElementXpaths>
      <isSelected>false</isSelected>
      <matchCondition>equals</matchCondition>
      <name>xpath:customAttributes</name>
      <type>Main</type>
      <value>//td[(text() = '
                                            Transfer QR Code / Bank (cek otomatis)
                                        ' or . = '
                                            Transfer QR Code / Bank (cek otomatis)
                                        ')]</value>
      <webElementGuid>0dd42537-6357-4b1d-9232-60d97deed7ed</webElementGuid>
   </webElementXpaths>
</WebElementEntity>
