import static com.kms.katalon.core.checkpoint.CheckpointFactory.findCheckpoint
import static com.kms.katalon.core.testcase.TestCaseFactory.findTestCase
import static com.kms.katalon.core.testdata.TestDataFactory.findTestData
import static com.kms.katalon.core.testobject.ObjectRepository.findTestObject
import static com.kms.katalon.core.testobject.ObjectRepository.findWindowsObject
import com.kms.katalon.core.checkpoint.Checkpoint as Checkpoint
import com.kms.katalon.core.cucumber.keyword.CucumberBuiltinKeywords as CucumberKW
import com.kms.katalon.core.mobile.keyword.MobileBuiltInKeywords as Mobile
import com.kms.katalon.core.model.FailureHandling as FailureHandling
import com.kms.katalon.core.testcase.TestCase as TestCase
import com.kms.katalon.core.testdata.TestData as TestData
import com.kms.katalon.core.testng.keyword.TestNGBuiltinKeywords as TestNGKW
import com.kms.katalon.core.testobject.TestObject as TestObject
import com.kms.katalon.core.webservice.keyword.WSBuiltInKeywords as WS
import com.kms.katalon.core.webui.keyword.WebUiBuiltInKeywords as WebUI
import com.kms.katalon.core.windows.keyword.WindowsBuiltinKeywords as Windows
import internal.GlobalVariable as GlobalVariable
import org.openqa.selenium.Keys as Keys

WebUI.callTestCase(findTestCase('REZA/Change Profile/Login Change Profil'), [:], FailureHandling.STOP_ON_FAILURE)

WebUI.click(findTestObject('REZA/Change Profile/TCCP 04 - mengubah nama valid/toggle_akun'))

WebUI.click(findTestObject('REZA/Change Profile/TCCP 04 - mengubah nama valid/menu_My Account'))

WebUI.click(findTestObject('REZA/Change Profile/TCCP 04 - mengubah nama valid/menu_Profil'))

WebUI.click(findTestObject('REZA/Change Profile/TCCP 04 - mengubah nama valid/menu_Edit Profile'))

WebUI.setText(findTestObject('REZA/Change Profile/TCCP 04 - mengubah nama valid/input_Fullname_name'), 'Muhammad Salah')

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Change Profile/TCCP 04 - mengubah nama valid/button_Save Changes'))

WebUI.verifyElementText(findTestObject('REZA/Change Profile/TCCP 04 - mengubah nama valid/div_Berhasil'), 'Berhasil')

WebUI.takeFullPageScreenshot()

WebUI.click(findTestObject('REZA/Change Profile/TCCP 04 - mengubah nama valid/button_OK'))

WebUI.takeFullPageScreenshot()

