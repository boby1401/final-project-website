<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test suite for Test Case</description>
   <name>TCCP 01 - 05</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>7ea9104b-d51a-428e-96c6-ed01c8f065ba</testSuiteGuid>
   <testCaseLink>
      <guid>1c8daef7-da7b-406f-bd53-f0b6997e41b5</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 01 - Verifikasi halaman menu profile</testCaseId>
      <testDataLink>
         <combinationType>ONE</combinationType>
         <id>d4783110-8239-4480-ae3f-c732ee5617a8</id>
         <iterationEntity>
            <iterationType>ALL</iterationType>
            <value></value>
         </iterationEntity>
         <testDataId>Data Files/TCCP 01</testDataId>
      </testDataLink>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId>d4783110-8239-4480-ae3f-c732ee5617a8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>email</value>
         <variableId>1397527b-8989-4e62-a39b-41cb812179a2</variableId>
      </variableLink>
      <variableLink>
         <testDataLinkId>d4783110-8239-4480-ae3f-c732ee5617a8</testDataLinkId>
         <type>DATA_COLUMN</type>
         <value>katasandi</value>
         <variableId>cb894606-ea9e-4a55-b92c-a3084b034921</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>6cf0da51-ad5a-4607-920e-b5b25aa07f94</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 02 - Verifikasi halaman edit profile</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>3e24a234-aeed-4a60-88fd-1616f17d0ac8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 03 - User mengubah foto profil dengan extensi foto valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
      <variableLink>
         <testDataLinkId></testDataLinkId>
         <type>DEFAULT</type>
         <value></value>
         <variableId>dd66d2ea-ab91-4987-842e-4f5ccd3b54d2</variableId>
      </variableLink>
   </testCaseLink>
   <testCaseLink>
      <guid>e0380089-2be0-46c9-b3e4-632fce81de6f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 04 - User mengubah nama valid</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>430a1ee8-0db1-406d-9cee-30897667ec50</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 05 - User mengubah nama lebih dari 30 karakter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
