<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test suite for Test Case</description>
   <name>TCCP 11 - 15</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>0046115c-5eee-4367-a934-b272cdcec038</testSuiteGuid>
   <testCaseLink>
      <guid>3a147c6c-ea06-4210-aaa1-0e1821ae4efc</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 11 - User mengubah nomor phone lebih dari 12 angka</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>13190bdf-2c21-4ee2-92de-d3b1c7dd0c95</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 12 - User menginput nomor whatsapp null</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>91a7de7a-dd18-48a3-978d-2096b19035c0</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 13 - User menginput no whtsapp dengan karakter</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>6b0508ec-4f85-4932-afd7-711d55519309</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 14 - User menginput no whtsapp dengan kode negara</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>1594eeed-7e35-4823-a9e7-3f9db807c37b</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Change Profile/TSCP 01 - Melakukan perubahan data profile/TCCP 15 - User menginput no whatsapp dengan alphabet dan angka</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
