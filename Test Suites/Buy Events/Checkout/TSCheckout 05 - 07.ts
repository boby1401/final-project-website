<?xml version="1.0" encoding="UTF-8"?>
<TestSuiteEntity>
   <description>Test suites for TSCart 05-07</description>
   <name>TSCheckout 05 - 07</name>
   <tag></tag>
   <isRerun>false</isRerun>
   <mailRecipient></mailRecipient>
   <numberOfRerun>0</numberOfRerun>
   <pageLoadTimeout>30</pageLoadTimeout>
   <pageLoadTimeoutDefault>true</pageLoadTimeoutDefault>
   <rerunFailedTestCasesOnly>false</rerunFailedTestCasesOnly>
   <rerunImmediately>false</rerunImmediately>
   <testSuiteGuid>24c57fbd-c152-459d-b1b6-43a6cba8df26</testSuiteGuid>
   <testCaseLink>
      <guid>928ed387-804c-4991-9ac4-cec038868fa8</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 05 - User menghapus event di checkout</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>403d3f53-d7e8-4043-8981-f87c2d0c5f8f</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 06 - Verifikasi halaman lihat semua events</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
   <testCaseLink>
      <guid>89118f32-1ff7-4249-a337-80e3fd482321</guid>
      <isReuseDriver>false</isReuseDriver>
      <isRun>true</isRun>
      <testCaseId>Test Cases/REZA/Buy Events/TSCheckout 01 - Verifikasi halaman checkout event/TCCheckout 07 - User melihat riwayat pembelian event</testCaseId>
      <usingDataBindingAtTestSuiteLevel>true</usingDataBindingAtTestSuiteLevel>
   </testCaseLink>
</TestSuiteEntity>
